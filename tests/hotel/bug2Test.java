package hotel;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.Null;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class bug2Test {
    
    Guest testGuest;
    Room testRoom;
    Hotel testHotel;
    Date testArrivalDate;
    int testStayLength;
    int testOccupants;
    CreditCard testCreditCard;
    ServiceType testServiceType;
    double testCost;
    double total;
    Booking testBooking;
    int roomID;
    public Map<Integer, Booking> activeBookingsByRoomId;
    Booking booking;
    
    
    @BeforeEach
    void setUp() throws Exception {
        Calendar myCalendar = new GregorianCalendar(2018, 11, 13);
        testArrivalDate = myCalendar.getTime();
        roomID =101;
        testRoom = new Room(roomID, RoomType.SINGLE);
        testStayLength = 1;
        testOccupants = 1;
        testGuest = new Guest("Emily", "10 Fake Street", 04);
        testCreditCard = new CreditCard(CreditCardType.MASTERCARD, 9999, 123);
        testBooking = new Booking(testGuest, testRoom, testArrivalDate, testStayLength, testOccupants, testCreditCard);
        testCost = 7.00;
        activeBookingsByRoomId = new HashMap<>();
        testHotel = HotelHelper.loadHotel();
    }
    
    @AfterEach
    void tearDown() {
    }
    
    @Test
    void test1() {
        //arrange
        testHotel.checkout(roomID);
        
        //act
        booking = testHotel.findActiveBookingByRoomId(roomID);
    
        //assert
        assertEquals(null, booking );
    }
}
