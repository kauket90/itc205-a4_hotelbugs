package hotel;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class bug1Test {
    
    Guest testGuest;
    Room testRoom;
    Date testArrivalDate;
    int testStayLength;
    int testOccupants;
    CreditCard testCreditCard;
    ServiceType testServiceType;
    double testCost;
    double total;
    Booking testBooking;
    
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        Calendar myCalendar = new GregorianCalendar(2018, 11, 13);
        testArrivalDate = myCalendar.getTime();
        testRoom = new Room(101, RoomType.SINGLE);
        testStayLength = 1;
        testOccupants = 1;
        testGuest = new Guest("Emily", "1 Fake Street", 04);
        testCreditCard = new CreditCard(CreditCardType.MASTERCARD, 9999, 123);
        testBooking = new Booking(testGuest, testRoom, testArrivalDate, testStayLength, testOccupants, testCreditCard);
        testCost = 7.00;
    }
    
    @AfterEach
    void tearDown() {
    }
    
    @Test
    void test1() {
        //arrange
        testServiceType = ServiceType.ROOM_SERVICE;
        testBooking.addServiceCharge(testServiceType, testCost);
        //act
        List<ServiceCharge> charges = testBooking.getCharges();
        for (ServiceCharge sc : charges) {
            total += sc.getCost();
            //assert
            assertEquals(7.00, total);
        }
    }
}
